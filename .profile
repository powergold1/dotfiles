export EDITOR=vim
export VISUAL=vim
export PAGER=less
export COLORTERM=truecolor
# fixes a djvulibre segfault for me
export LIBDJVU_DISABLE_MMX=1
# export QT_QPA_PLATFORMTHEME=gtk2
export SMLNJ_HOME="${HOME}/builds/smlnj"
export GOPATH="${HOME}/go"
#export GOROOT="${HOME}/builds/go"
export PATH="${HOME}/scripts:${SMLNJ_HOME}/bin:${CARGOBIN}:${GOPATH}/bin:${HOME}/.local/bin:${PATH}"
export STEAM_COMPAT_DATA_PATH="${HOME}/.steam/steam/steamapps/compatdata"
export VST_PATH="${HOME}/vst"
export LYNX_LSS="${HOME}/lynx.lss"
export PROMPT_COMMAND=""
export ENV="$HOME/.kshrc"
export MC_XDG_OPEN="$HOME/scripts/nohup-open"

export CC="gcc"
export CXX="g++"
export CPP="gcc -E"
export LD="ld"
export CFLAGS="-march=native -mtune=native -O2 -pipe -flto -fno-plt"
export CXXFLAGS="-march=native -mtune=native -O2 -pipe -flto -fno-plt"

eval $(keychain --noask --eval id_rsa 2> /dev/null)
