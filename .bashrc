#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
[ -z "$PS1" ] && return

setTitle() {
  echo -e "\033]0;$@\007"
}

up() {
	cd $(printf "%0.s../" $(seq 1 $1));
}

alias ls='ls --color=never -p'
alias slp="sudo systemctl suspend"
alias Syu="yay -Syu"
alias Rs="sudo pacman -Rs"
alias news="newsboat"
alias tn="date -u +%s"
alias mk="~/builds/9base/mk/mk"
alias ytd="youtube-dl -i -q --no-mtime -x -o \"%(title)s.%(ext)s\""
alias weechat="setTitle weechat; weechat"
alias irssi="setTitle irssi; irssi"
alias fsharpc="fsharpc --nologo"
alias qc="qalc"
alias proton="~/.steam/steam/steamapps/common/Proton\ 5.0/proton"
alias mpv1="mpv --ytdl-format=1"



# PS1='[\u@\h \W]\$ '
PS1='\w \$ '
