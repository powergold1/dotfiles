if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
		https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'Shougo/deoplete.nvim'
Plug 'SirVer/ultisnips'
Plug 'preservim/nerdtree'
call plug#end()

set nocompatible
set nomodeline
syntax off
set autoindent
" allow backspacing over autoindent, line breaks, and the start of insert
set backspace=indent,eol,start
" don't scan included files for completion
set complete-=i
filetype plugin indent on
set encoding=utf8
set nowrap
set nu
set ruler
set mouse=a
set ttymouse=xterm
set hlsearch
set noincsearch
set laststatus=2
set path+=**
set wildmenu
set hidden
set guifont=DejaVu\ Sans\ Mono\ 12
set guioptions-=m
set guioptions-=T
set guioptions-=L " scroll bar left
set guicursor+=a:blinkon0
set background=dark
set t_Co=256
" the scheme
hi LineNr       cterm=NONE	ctermfg=None
hi CursorLineNr cterm=NONE	ctermfg=None
hi Cursor       cterm=NONE	ctermfg=232	ctermbg=227
hi Search       cterm=NONE	ctermfg=232	ctermbg=137
hi CursorLine   cterm=NONE	ctermfg=None	ctermbg=15
hi StatusLine	cterm=NONE	ctermfg=232	ctermbg=137
hi StatusLineNC	cterm=NONE	ctermfg=232	ctermbg=239
" set cursorline
" new split panes open to bottom right
set splitbelow
set splitright
set backupdir=/home/aru/vimbackups/
set dir=/home/aru/vimbackups/vimswaps/

" tab in front of a line inserts blanks according to shiftwidth
set smarttab
set tabstop=8       " Sets width of tabs
" set shiftwidth=2    " Sets indent width
" set softtabstop=2   " Sets the number of columns when tab is pressed
" set expandtab       " Expand TABs to spaces

vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+

nnoremap <C-t> :tabnew<CR>
nnoremap <A-t> :tabnew<CR>
nnoremap <C-x> :tabclose<CR>
nnoremap <C-h> :tabprevious<CR>
nnoremap <C-l> :tabnext<CR>
nnoremap <C-k> :tabm -1<CR>
nnoremap <C-j> :tabm +1<CR>

let g:Tlist_GainFocus_On_ToggleOpen = 1
nnoremap <silent><F4>  :TlistToggle<CR>

let g:clang_cpp_options = '-std=c++11'
" let g:clang_c_options = '-std=c11'
let g:clang_auto = 0
let g:clang_cpp_completeopt = 'preview'
let g:clang_c_completeopt = 'preview'

let g:deoplete#enable_at_startup = 1
let g:deoplete#sources#d#dcd_server_autostart = 0
inoremap <expr><C-h> deoplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS> deoplete#smart_close_popup()."\<C-h>"
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<tab>"
"let g:deoplete#enable_smart_case = 1
let g:deoplete#enable_auto_select = 0
inoremap <expr><C-l> deoplete#complete_common_string()
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function()
	"complete and do the normal cr function of creating a new line
	  return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
	"only do the completion but don't create a new line
	" return pumvisible() ? "\<C-y>" : "\<CR>"
endfunction

" ctrl tab, because ultisnips conflicts with deoplete otherwise
" and I couldn't figure out how to fix that
let g:UltiSnipsExpandTrigger="<c-tab>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsJumpEditSplit="vertical"

function! <SID>StripTrailingWhitespace()
	" preserve last search and cursor position
	let _s=@/
	let l = line(".")
	let c = col(".")
	%s/\s\+$//e
	let @/=_s
	call cursor(l,c)
endfunction

autocmd BufWritePre * :call <SID>StripTrailingWhitespace()
