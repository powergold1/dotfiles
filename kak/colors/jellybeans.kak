# jelly theme
# Comments are red, because I should put more value in them
 
%sh{
    jelly_white="rgb:e8e8d3"
    jelly_grey="rgb:3c3c3c"
    jelly_dark_grey="rgb:1f1f1f"
    jelly_darker_grey="rgb:151515"
    jelly_lighter_grey="rgb:888888"
    jelly_orange="rgb:ffb964"
    jelly_light_yellow="rgb:eaeaaa"
    jelly_blue="rgb:8197bf"
    jelly_dark_blue="rgb:8147b7"
    jelly_green="rgb:99ad6a"
    jelly_bright_green="rgb:a9cf6a"
    jelly_dark_green="rgb:295020"
    jelly_magenta="rgb:bc5766"
    jelly_red="rgb:cf6a4c"
    jelly_light_red="rgb:bc5f66"
    jelly_dark_red="rgb:7a4b46"
 
    echo "
        # then we map them to code
        face value ${jelly_red}
        face type ${jelly_orange}
        face identifier "rgb:c6b6ee"
        face string ${jelly_green}
        face error "rgb:888888"
        face keyword ${jelly_blue}
        face operator "rgb:888888"
        face attribute "rgb:cfa964"
        face comment ${jelly_light_red}
        face meta "rgb:e8e8d3"
 
 
        # and markup
        face title "rgb:888888"
        face header "rgb:888888"
        face bold "rgb:888888"
        face italic "rgb:888888"
        face mono "rgb:888888"
        face block  "rgb:888888"
        face link "rgb:888888"
        face bullet "rgb:888888"
        face list "rgb:888888"
 
        # and built in faces
        face Default ${jelly_white},${jelly_darker_grey}
        face PrimarySelection ${jelly_darker_grey},"rgb:8fbfdc"
        face SecondarySelection  ${jelly_lighter_grey},"rgb:c6b6ee"
        face PrimaryCursor ${jelly_darker_grey},${jelly_magenta}
        face SecondaryCursor ${jelly_darker_grey},${jelly_magenta}
        face LineNumbers ${jelly_grey},${jelly_dark_grey}
        face LineNumberCursor ${jelly_grey},${jelly_dark_grey}+b
        face MenuBackground ${jelly_light_yellow},${jelly_dark_grey}
        face MenuForeground ${jelly_dark_grey},${jelly_light_yellow}
        face MenuInfo ${jelly_grey}
        face Information ${jelly_white},${jelly_dark_green}
        face Error ${jelly_light_red},${jelly_dark_red}
        face StatusLine ${jelly_lighter_grey},${jelly_dark_grey}
        face StatusLineMode ${jelly_lighter_grey},${jelly_dark_green}+b
        face StatusLineInfo ${jelly_dark_grey},${jelly_lighter_grey}
        face StatusLineValue ${jelly_lighter_grey}
        face StatusCursor default,${jelly_blue}
        face Prompt ${jelly_lighter_grey}
        face MatchingChar ${jelly_lighter_grey},${jelly_bright_green}
		face BufferPadding ${jelly_green},${jelly_darker_grey}
    "
}
