-- Author: aru
-- Appends url from clipboard to the playlist.
-- Requires xsel, only runs on linux.
--
-- base script copied and edited from:
-- donmaiq
-- https://github.com/jonniek/mpv-scripts/blob/master/appendURL.lua

-- Permission to use, copy, modify, and/or distribute this software for any
-- purpose with or without fee is hereby granted, provided that the above
-- copyright notice and this permission notice appear in all copies.

local utils = require 'mp.utils'
local msg = require 'mp.msg'

function append(primaryselect)
  local clipboard = get_clipboard(primaryselect or false)
  if clipboard then
    mp.commandv("loadfile", clipboard, "append-play")
    mp.osd_message("URL appended: "..clipboard)
    msg.info("URL appended: "..clipboard)
  end
end

function get_clipboard(primary)
  local args = { 'xsel', primary and '' or '-b', '-o' }
  return handleres(utils.subprocess({ args = args }), args, primary)
end

function handleres(res, args, primary)
  if not res.error and res.status == 0 then
      return res.stdout
  else
    --if clipboard failed try primary selection
    if not primary then
      append(true)
      return nil
    end
    msg.error("There was an error getting "..platform.." clipboard: ")
    msg.error("  Status: "..(res.status or ""))
    msg.error("  Error: "..(res.error or ""))
    msg.error("  stdout: "..(res.stdout or ""))
    msg.error("args: "..utils.to_string(args))
    return nil
  end
end

mp.add_key_binding("a", "appendURL", append)
