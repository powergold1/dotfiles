
set background=light
highlight clear
if exists("syntax_on")
    syntax reset
endif

let colors_name = "unsyntax"

hi Normal ctermfg=NONE ctermbg=NONE guifg=Black    guibg=LightYellow

" Syntax highlighting (other color-groups using default, see :help group-name):
hi Comment    cterm=NONE ctermfg=DarkGray
hi Constant   cterm=NONE ctermfg=19
hi Identifier cterm=NONE ctermfg=19
hi Function   cterm=NONE ctermfg=19
hi Statement  cterm=bold ctermfg=19
hi PreProc    cterm=NONE ctermfg=NONE
hi Type       cterm=NONE ctermfg=18
hi Special    cterm=NONE ctermfg=NONE
hi Delimiter  cterm=NONE ctermfg=NONE
hi String     cterm=NONE ctermfg=28
hi Error      cterm=NONE ctermfg=DarkRed        ctermbg=NONE
hi LineNr     cterm=NONE ctermfg=DarkYellow
hi Search     cterm=NONE ctermfg=NONE           ctermbg=Yellow
hi MatchParen cterm=NONE ctermfg=Magenta        ctermbg=NONE
hi CursorLine cterm=NONE ctermbg=193
