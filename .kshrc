[[ $- != *i* ]] && return
[ -z "$PS1" ] && return

HISTFILE="$HOME/.ksh_history"
HISTSIZE=5000

setTitle() {
	echo -e "\033]0;$@\007"
}

up() {
	cd $(printf "%0.s../" $(seq 1 $1));
}

alias ls='ls --color=never -p'
alias slp="sudo systemctl suspend"
alias news="newsboat"
alias tn="date -u +%s"
alias mk="~/builds/9base/mk/mk"
alias ytd="youtube-dlc -i -q --no-mtime -x -o \"%(title)s.%(ext)s\""
alias irssi="setTitle irssi; irssi"
alias fsharpc="fsharpc --nologo"
alias qc="qalc"
alias proton="~/.steam/steam/steamapps/common/Proton\ 5.0/proton"
alias mpv1="mpv --ytdl-format=1"

set -o emacs

PS1='\w \$ '
