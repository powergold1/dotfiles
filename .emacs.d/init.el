;; -*- lexical-binding: t -*-

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(package-initialize)

(add-to-list 'default-frame-alist '(height . 40))
(add-to-list 'default-frame-alist '(width . 100))

(require 'use-package)
(prefer-coding-system 'utf-8-unix)

(use-package expand-region
  :ensure t)

;; very useful to have in dvorak C-t is just transpose-chars by
;; default, so nothing of value is lost really
;; (define-key key-translation-map "\C-t" "\C-x")

(require 'mouse)
(xterm-mouse-mode t)

(use-package swap-buffers
  :ensure t
  :bind
  ("C-c b" . swap-buffers))

(use-package neotree
  :ensure t
  :bind
  ("<f8>" . 'neotree-toggle))

(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1)
  :config
  (setq yas-snippet-dirs
        '("~/.emacs.d/snippets" ))
  :bind
  (:map yas-minor-mode-map
  ("<tab>" . nil)
  ("TAB" . nil)
  ("C-l" . yas-expand)
  :map yas-keymap
  ("TAB" . yas-next-field-or-maybe-expand)
  ("S-TAB" . yas-prev)))

;; (use-package realgud
;;   :ensure t)

;; better default indentation
(use-package stupid-indent-mode
  :ensure t
  :config
  (setq stupid-indent-level 2)
  (add-hook 'text-mode-hook 'stupid-indent-mode)
  (add-hook 'csv-mode-hook (lambda () (stupid-indent-mode 0)))
  (add-hook 'sml-mode-hook 'stupid-indent-mode)
  (add-hook 'fsharp-mode-hook 'stupid-indent-mode))

(setq standard-indent 2)
(setq-default indent-tabs-mode t
              fill-column 100
	      python-indent-offset 4
	      electric-indent-inhibit t
	      tab-width 8
	      sh-basic-offset 8
	      c-basic-offset 8)
(setq backward-delete-char-untabify-method 'hungry)

(add-hook 'sml-mode-hook '(lambda () (setq indent-tabs-mode nil)))

;; smoother scrolling
(setq mouse-wheel-scroll-amount '(4 ((shift) . 4)) ;; this many lines at a time
      mouse-wheel-progressive-speed nil ;; no acceleration
      mouse-wheel-follow-mouse 't ;; scroll the window under mouse
      scroll-step 1 ;; keyboard scroll one at a time
      scroll-conservatively 100 ;; don't shift the screen down by more than one line
      auto-window-vscroll nil ;; don't care about lines that are larger because they contain images or anything
      jit-lock-defer-time 0.05
      fast-but-imprecise-srolling t) ;; significantly reduces lag when scrolling

;; shell
(defvar my-term-shell "/bin/bash")
(defadvice ansi-term (before force-bash)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)
(global-set-key (kbd "<s-return>") 'ansi-term)

(use-package projectile
  :ensure t
  :bind
  (:map projectile-mode-map ("C-;" . projectile-command-map)))

(use-package ivy
  :ensure t
  :diminish ivy-mode
  :init
  (setq ivy-height 12
        ivy-use-virtual-buffers nil
        ivy-count-format "(%d/%d) ")
  (ivy-mode 1))

(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

(use-package amx
  :ensure t
  :after ivy
  :custom
  (amx-backend 'auto)
  (amx-save-file "~/.emacs.d/amx-items")
  (amx-history-length 50)
  :config
  (amx-mode 1))

(setq save-interprogram-paste-before-kill t)

(use-package counsel-projectile
  :ensure t
  :bind
  (("C-c C-;" . counsel-projectile-mode)))

(use-package projectile-ripgrep
  :ensure t)

(use-package dumb-jump
  :ensure t
  :config
  (setq dumb-jump-selector 'ivy
  dumb-jump-prefer-searcher 'rg)
  :bind
  (("C-M-o" . dumb-jump-go-other-window)
   ("C-M-g" . dumb-jump-go)
   ("C-M-p" . dumb-jump-back)))


(use-package avy
  :ensure t
  :init
  (setq avy-timeout-seconds 0.2)
  :bind ("C-." . avy-goto-char-2)
  :bind ("C-," . avy-goto-char-timer))

(recentf-mode 1)
(setq recentf-max-menu-items 32)
(setq recentf-max-saved-items 32)
(global-set-key (kbd "C-x C-r") 'recentf-open-files)

;; backups
(setq backup-by-copying t
      backup-directory-alist
      '(("." . "~/.emacsbackups"))
      delete-old-version t
      kept-new-versions 6
      kept-old-versions 2
      auto-save-interval 1000
      auto-save-timeout 600)

;; appearance and other settings
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 1)
(menu-bar-right-scroll-bar)
(show-paren-mode t)
(put 'upcase-region 'disabled nil)
(setq show-paren-delay 0
      inhibit-startup-message t ;; don't show the default startup buffer
      column-number-mode t ;; show current column in addition to the current line
      ring-bell-function 'ignore
      gdb-many-windows t
      dired-dwim-mode t
      require-final-newline nil
      browse-url-browser-function 'browse-url-firefox)
(global-font-lock-mode 0)
(setq font-lock-support-mode 'jit-lock-mode
      jit-lock-stealth-time 16
      jit-lock-defer-contextually t
      jit-lock-stealth-nice 0.5)
(setq-default font-lock-multiline t)
(global-linum-mode 0)
(setq-default truncate-lines t ;; t for no line wrapping
              cursor-type 'box)
(blink-cursor-mode 0)
(windmove-default-keybindings)
(global-hl-line-mode -1)
(global-prettify-symbols-mode -1)
(defalias 'yes-or-no-p 'y-or-n-p)
(setq vc-follow-symlinks nil)
(setq-default show-trailing-whitespace t)
;; (add-hook 'before-save-hook 'delete-trailing-whitespace)

(superword-mode t)
(delete-selection-mode 1)
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
(load-theme 'schism t)

;; fonts
(set-face-attribute
 'default nil
 :family "dejavu sans mono"
 :weight 'regular
 :height 90)
(set-face-attribute
 'variable-pitch nil
 :family "dejavu sans"
 :weight 'normal
 :height 105)

;; proof general
(use-package proof-general
  :ensure t
  :config
  (setq-default proof-splash-enable nil
                proof-three-window-mode-policy 'hybrid))

(use-package undo-tree
  :ensure t
  :init
  (global-undo-tree-mode 1))

;; (use-package company
;;   :ensure t
;;   :init
;;   (setq company-dabbrev-downcase nil
;;         company-auto-complete nil
;;         company-idle-delay 0
;;         company-require-match 'never  ;; non matching characters cancel selections
;;         company-frontends '(company-pseudo-tooltip-frontend
;;                             company-echo-metadata-frontend))
;;   ;;company-preview-frontend
;;   ;;company-echo-metadata-frontend))
;;   ;;(global-company-mode)
;;   :config
;;   (setq company-minimum-prefix-length 3
;;         company-auto-complete-chars nil)
;;   (setq company-backends
;;         '(company-files
;;           company-capf
;;           (company-keywords company-etags company-dabbrev-code)
;;           (company-abbrev company-dabbrev)))
;;   ;;(company-ac-setup)
;;   :bind
;;   (("M-n" . company-manual-begin)
;;    :map company-active-map
;;    ("C-SPC" . company-complete-selection)
;;    ("C-." . company-complete-selection)
;;    ("TAB" . company-complete-common-or-cycle)
;;    ("<tab>" . company-complete-common-or-cycle)
;;    ("S-TAB" . company-select-previous)
;;    ("<backtab>" . company-select-previous)
;;    :map company-active-map
;;    :filter (company-explicit-action-p)
;;    ;; this way return only triggers a completion if we explicitly
;;    ;; used company before (by pressing tab for instance).
;;    ;; otherwise we just insert a newline.
;;    ("<return>" . company-complete-selection)
;;    ("RET" . company-complete-selection)))

(defun c-lineup-arglist-tabs-only (ignored)
  "Line up argument lists by tabs, not spaces"
  (let* ((anchor (c-langelem-pos c-syntactic-element))
         (column (c-langelem-2nd-pos c-syntactic-element))
         (offset (- (1+ column) anchor))
         (steps (floor offset c-basic-offset)))
    (* (max steps 1)
       c-basic-offset)))

(c-add-style "my-c-style"
       '((c-basic-offset . 8)
         (c-comment-only-line-offset . 0)
         (c-offsets-alist
    (statement-block-intro . +)
    (knr-argdecl-intro . +)
    (substatement-open . 0)
    (substatement-label . 0)
    (label . 0)
    (statement-cont . +)
    (inline-open . 0)
    (innamespace . [0])
    (inexpr-class . 0))))


(c-add-style "kernel"
             '("linux"
               (c-offsets-alist
		(arglist-cont-nonempty
                 c-lineup-gcc-asm-reg
                 c-lineup-arglist-tabs-only))))

(defun my-c-hook ()
  (setq comment-start "//"
        comment-end "")
  (define-key c-mode-base-map (kbd "C-c C-l") 'compile)
  (setq indent-tabs-mode t)
  ;; (define-key c-mode-map (kbd "<tab>") 'my-insert-tab)
  ;; (define-key c++-mode-map (kbd "<tab>") 'my-insert-tab)
  (setq-default c-tab-always-indent t)
  (c-set-style "kernel"))

;; C/C++
(add-hook 'c-mode-hook #'my-c-hook)
(add-hook 'c++-mode-hook #'my-c-hook)
(setq-default c-default-style "my-c-style")

;; D
(use-package d-mode
  :ensure t)

;; (use-package company-dcd
;;   :config
;;   (add-hook 'd-mode-hook 'company-dcd-mode))

;; Zig
(use-package zig-mode
  :ensure t)

;; ;; F#
;; (use-package fsharp-mode
;;   :ensure t
;;   :defer t)

;; Common Lisp
(use-package sly
  :ensure t
  :defer t
  :config
  (setq inferior-lisp-program "sbcl"))

;; PDDL
(add-to-list 'auto-mode-alist '("\\.pddl\\'" . text-mode))

(use-package markdown-mode
  :defer t
  :ensure t)

;; latex / auctex
(use-package flyspell
  :ensure t
  :defer t)
(setq TeX-auto-save t
      TeX-parse-self t
      ;;TeX-view-program-list '(("Zathura" "zathura -P %(outpage) %o"))
      ;;TeX-view-program-selection '((output-pdf "Zathura"))
      TeX-view-program-list '(("qpdfview" "qpdfview %o"))
      TeX-view-program-selection '((output-pdf "qpdfview"))
      font-latex-fontify-script nil
      safe-local-variable-values (quote ((TeX-master . t))))
(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'visual-line-mode)
;; (add-hook 'LaTeX-mode-hook 'auto-fill-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
;; (add-hook 'LaTeX-mode-hook '(lambda () (variable-pitch-mode 1)))

;; my functions
(defun my-scroll-half-page (direction)
  "Scrolls half page up if `direction' is non-nil, otherwise will scroll half page down."
  (let ((opos (cdr (nth 6 (posn-at-point)))))
    ;; opos = original position line relative to window
    (move-to-window-line nil)  ;; Move cursor to middle line
    (if direction
        (recenter-top-bottom -1)  ;; Current line becomes last
      (recenter-top-bottom 0))  ;; Current line becomes first
    (move-to-window-line opos)))  ;; Restore cursor/point position

(defun my-scroll-half-page-down ()
  "Scrolls exactly half page down keeping cursor/point position."
  (interactive)
  (my-scroll-half-page nil))

(defun my-scroll-half-page-up ()
  "Scrolls exactly half page up keeping cursor/point position."
  (interactive)
  (my-scroll-half-page t))

(defun my-newline ()
  "Inserts a line break at the end of the line."
  (interactive)
  (move-end-of-line nil)
  (newline))

(defun die-tabs ()
  (interactive)
  (set-variable 'tab-width 2)
  (mark-whole-buffer)
  (untabify (region-beginning) (region-end))
  (keyboard-quit))

(defun my-insert-tab ()
  (interactive)
  (insert-char 9))



;; Key bindings
;; (global-set-key (kbd "C-d") 'my-scroll-half-page-down)
;; (global-set-key (kbd "C-u") 'my-scroll-half-page-up)
;; (global-set-key (kbd "C-S-u") 'universal-argument)
;; (global-set-key (kbd "<C-tab>") 'indent-for-tab-command)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-<return>") 'my-newline)
(global-set-key (kbd "C-/") 'dabbrev-expand)
(global-set-key (kbd "M-/") 'dabbrev-completion)
(define-key undo-tree-map (kbd "C-/") 'dabbrev-expand)
(define-key undo-tree-map (kbd "M-/") 'dabbrev-completion)
(global-set-key (kbd "M-?") 'universal-argument)

(global-set-key (kbd "M-h") 'ff-find-other-file)
(global-set-key [remap comment-dwim] #'comment-line)
(global-set-key (kbd "C-x C-b") 'ibuffer)
;; (global-set-key (kbd "M-j") 'imenu)
(global-set-key (kbd "M-J") 'join-line)
(global-set-key (kbd "C-w") 'kill-region)
(global-set-key (kbd "C-q") 'copy-region-as-kill)
(global-set-key (kbd "C-f") 'yank)
(global-set-key (kbd "C-v") 'er/expand-region)
(global-set-key (kbd "M-u") 'undo)
(global-set-key (kbd "M-U") 'redo)
(global-set-key (kbd "M-l") 'query-replace)
(global-set-key (kbd "M-L") 'replace-string)
(global-set-key (kbd "M-r") 'replace-regexp)
(global-set-key (kbd "M-g") 'goto-line)
(global-set-key (kbd "M-e") 'next-error)
(global-set-key (kbd "M-E") 'previous-error)
(global-set-key (kbd "M-|") 'pop-to-mark-command)
(global-set-key (kbd "C-S-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-S-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-S-<up>") 'enlarge-window)
(global-set-key (kbd "C-S-<down>") 'shrink-window)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(amx-backend 'auto)
 '(amx-history-length 50)
 '(amx-save-file "~/.emacs.d/amx-items")
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#000000" "#990000" "#339900" "#666633" "#0000C8" "#9933FF" "#666633" "#000000"])
 '(ansi-term-color-vector
   [unspecified "#000000" "#ff4450" "#c0ffdd" "#ffedc0" "#71ddff" "#dda0ff" "#71ddff" "#e0e0e0"] t)
 '(beacon-color "#f2777a")
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(fci-rule-character-color "#1c1c1c")
 '(flycheck-color-mode-line-face-to-color 'mode-line-buffer-id)
 '(frame-background-mode 'dark)
 '(fringe-mode 6 nil (fringe))
 '(hl-paren-background-colors '("#e8fce8" "#c1e7f8" "#f8e8e8"))
 '(hl-paren-colors '("#40883f" "#0287c8" "#b85c57"))
 '(linum-format 'dynamic)
 '(nrepl-message-colors
   '("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3"))
 '(package-selected-packages
   '(csv-mode glsl-mode pylint colorless-themes zig-mode amx cider sly yasnippet company parinfer counsel-projectile counsel ivy projectile-ripgrep projectile go-mode dumb-jump d-mode mu4e magit expand-region load-dir proof-general undo-tree stupid-indent-mode markdown-mode neotree avy-menu rg swap-buffers use-package avy highlight-current-line pdf-tools sml-mode auto-complete auctex))
 '(pdf-view-midnight-colors '("#DCDCCC" . "#383838"))
 '(scroll-bar-mode 'right)
 '(sml/active-background-color "#98ece8")
 '(sml/active-foreground-color "#424242")
 '(sml/inactive-background-color "#4fa8a8")
 '(sml/inactive-foreground-color "#424242")
 '(window-divider-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
