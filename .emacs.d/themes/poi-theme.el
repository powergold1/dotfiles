(deftheme poi "My poi theme")

(let ((class '((class color) (min-colors 89)))
      ;; poi palette
      (poi-fg             "#e8e8d3")
      (poi-pink           "#fc6786")
      (poi-red            "#de5577")
      (poi-palered        "#f37090")
      (poi-orange         "#fad07a")
      (poi-wheat          "#c4be89")
      (poi-lightgold      "#eed8a6")
      (poi-darkkhaki      "#bdb76b")
      (poi-blue           "#9797f8")
      (poi-lightblue      "#b0b0f8")
      (poi-darkblue       "#8197bf")
      (poi-green          "#99bd6a")
      (poi-palegreen      "#caeab0")
      (poi-darkwine       "#1e0010")
      (poi-wine           "#960050")
      (poi-purple         "#ae81ff")
      (poi-palevioletred  "#e34682")
      (poi-grey-2         "#bcbcbc")
      (poi-grey           "#a0a0a0")
      (poi-grey+2         "#403d3d")
      (poi-grey+5         "#232526")
      (poi-grey50         "gray50")
      (poi-bg             "#1c1c1c")
      ;;(poi-bg             "#000000")
      (poi-linebg         "#202030")
      ;;(poi-linebg         "#203020")
      (poi-dark           "#000000")
      (poi-base01         "#465457")
      (poi-base02         "#455354")
      (poi-base03         "#28283e"))
  (custom-theme-set-faces
   'poi

   ;; base
   `(default ((t (:background ,poi-bg :foreground ,poi-fg))))
   `(cursor ((t (:background ,poi-fg :foreground ,poi-bg))))
   `(fringe ((t (:foreground ,poi-base02 :background ,poi-bg))))
   `(highlight ((t (:background ,poi-grey))))
   `(region ((t (:background  ,poi-grey-2 :foreground ,poi-bg))))
   `(warning ((t (:foreground ,poi-palevioletred))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,poi-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,poi-blue))))
   `(font-lock-comment-face ((t (:foreground ,poi-grey50))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,poi-grey))))
   `(font-lock-constant-face ((t (:foreground ,poi-orange))))
   `(font-lock-doc-string-face ((t (:foreground ,poi-grey50))))
   `(font-lock-function-name-face ((t (:foreground ,poi-fg))))
   `(font-lock-keyword-face ((t (:foreground ,poi-blue ))))
   `(font-lock-negation-char-face ((t (:foreground ,poi-fg))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,poi-green))))
   `(font-lock-type-face ((t (:foreground ,poi-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,poi-fg))))
   `(font-lock-warning-face ((t (:foreground ,poi-palevioletred))))

   ;; mode line
   `(mode-line ((t (:foreground ,poi-fg
                                :background ,poi-base03
                                :box nil))))
   `(mode-line-buffer-id ((t (:weight normal))))
   `(mode-line-inactive ((t (:foreground ,poi-fg
                                         :background ,poi-base02
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,poi-dark :background ,poi-wheat :weight normal))))
   `(isearch-fail ((t (:foreground ,poi-wine :background ,poi-darkwine))))

   ;; linum-mode
   `(linum ((t (:foreground ,poi-grey-2 :background ,poi-bg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,poi-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,poi-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,poi-linebg)) (t : weight normal)))
   
   ;; company-mode
   `(company-preview-common ((,class (:foreground nil :background ,poi-wine))))
   `(company-scrollbar-bg ((,class (:background ,poi-grey+5))))
   `(company-scrollbar-fg ((,class (:background ,poi-base03))))
   `(company-tooltip ((,class (:foreground ,poi-fg :background ,poi-grey+5))))
   `(company-tooltip-common ((,class (:foreground ,poi-wheat :background ,poi-grey+5))))
   `(company-tooltip-common-selection ((,class (:foreground ,poi-wheat :background ,poi-wine))))
   `(company-tooltip-selection ((,class (:background ,poi-wine))))
   `(company-tooltip-annotation ((,class (:foreground ,poi-wheat :background ,poi-grey+5))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,poi-fg :background ,poi-wine))))

   ;; ido
   `(ido-first-match ((,class (:foreground ,poi-fg :weight bold))))
   `(ido-only-match ((,class (:foreground ,poi-fg :weight bold ))))
   `(ido-subdir ((,class (:foreground ,poi-blue))))
   `(ido-indicator ((,class (:weight bold :foreground ,poi-fg))))
   `(ido-virtual ((,class (:weight bold :foreground ,poi-fg))))
   `(ido-incomplete-regexp ((,class (:weight bold :foreground ,poi-fg))))
   
   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,poi-darkkhaki :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,poi-orange))))
   `(font-latex-italic-face ((t (:foreground ,poi-blue :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,poi-lightgold))))

   ;; coq
   
   `(proof-tacticals-name-face ((t (:foreground ,poi-darkblue))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,poi-lightblue))))
   
   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom poi-theme-kit nil
  "Non-nil means load poi-theme-kit UI component"
  :type 'boolean
  :group 'poi-theme)

(defcustom poi-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "poi-theme-kit.el")
  "poi-theme-kit-file"
  :type 'string
  :group 'poi-theme)

(if (and poi-theme-kit
         (file-exists-p poi-theme-kit-file))
    (load-file poi-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'poi)

;;; poi-theme.el ends here
