(deftheme iop-light "My iop-light theme")

(let ((class '((class color) (min-colors 89)))
      ;; iop-light palette
      (iop-fg             "#000000")
      (iop-fg2            "#000058")
      (iop-fg3            "#004810")
      (iop-fg4            "#700020")
      (iop-dark           "#000000")
      (iop-bg             "#e6e6d3")
      (iop-linebg         "#e8e8e8")
      (iop-grey-2         "#bcbcbc")
      (iop-grey           "#a0a0a0")
      (iop-grey+2         "#403d3d")
      (iop-grey+5         "#a3a5a6")
      ;;(iop-commentfg      "#703010")
      (iop-commentfg      "#503050")

      (iop-base01         "#c6d4d7")
      (iop-base02         "#d5d3d4")
      (iop-base03         "#a8c6a8"))
  (custom-theme-set-faces
   'iop-light

   ;; base
   `(default ((t (:background ,iop-bg :foreground ,iop-fg))))
   `(cursor ((t (:background ,iop-fg :foreground ,iop-bg))))
   `(fringe ((t (:foreground ,iop-base02 :background ,iop-bg))))
   `(highlight ((t (:background ,iop-grey))))
   `(region ((t (:background  ,iop-grey-2 :foreground ,iop-fg))))
   `(warning ((t (:foreground ,iop-fg))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,iop-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,iop-fg2))))
   `(font-lock-comment-face ((t (:foreground ,iop-commentfg))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,iop-commentfg))))
   `(font-lock-constant-face ((t (:foreground ,iop-fg2))))
   `(font-lock-doc-string-face ((t (:foreground ,iop-fg3))))
   `(font-lock-function-name-face ((t (:foreground ,iop-fg2))))
   `(font-lock-keyword-face ((t (:foreground ,iop-fg2 ))))
   `(font-lock-negation-char-face ((t (:foreground ,iop-fg2))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,iop-fg3))))
   `(font-lock-type-face ((t (:foreground ,iop-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,iop-fg))))
   `(font-lock-warning-face ((t (:foreground ,iop-fg2))))

   ;; mode line
   `(mode-line ((t (:foreground ,iop-fg
                                :background ,iop-base03
                                :box nil))))
   `(mode-line-inactive ((t (:foreground ,iop-fg
                                         :background ,iop-base02
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,iop-bg :background ,iop-grey+2 :weight normal))))
   `(isearch-fail ((t (:foreground ,iop-fg4 :background ,iop-bg))))

   ;; linum-mode
   `(linum ((t (:foreground ,iop-grey-2 :background ,iop-bg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,iop-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,iop-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,iop-linebg)) (t : weight normal)))

   ;; company-mode
   `(company-preview-common ((,class (:foreground ,iop-fg :background ,iop-bg))))
   `(company-scrollbar-bg ((,class (:background ,iop-grey+5))))
   `(company-scrollbar-fg ((,class (:background ,iop-base03))))
   `(company-tooltip ((,class (:foreground ,iop-fg :background ,iop-grey+5))))
   `(company-tooltip-common ((,class (:foreground ,iop-fg3 :background ,iop-grey+5))))
   `(company-tooltip-common-selection ((,class (:foreground ,iop-bg :background ,iop-fg3))))
   `(company-tooltip-selection ((,class (:foreground ,iop-bg :background ,iop-fg3))))
   `(company-tooltip-annotation ((,class (:foreground ,iop-fg :background ,iop-grey+5))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,iop-bg :background ,iop-fg3))))

   ;; ido
   `(ido-first-match ((,class (:foreground ,iop-fg :weight bold))))
   `(ido-only-match ((,class (:foreground ,iop-fg :weight bold ))))
   `(ido-subdir ((,class (:foreground ,iop-fg2))))
   `(ido-indicator ((,class (:weight bold :foreground ,iop-fg))))
   `(ido-virtual ((,class (:weight bold :foreground ,iop-fg))))
   `(ido-incomplete-regexp ((,class (:weight bold :foreground ,iop-fg))))
   
   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,iop-fg3 :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,iop-fg3))))
   `(font-latex-italic-face ((t (:foreground ,iop-fg2 :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,iop-fg3))))

   ;; coq
   `(proof-tacticals-name-face ((t (:foreground ,iop-fg2))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,iop-fg2))))

   ;; slime
   `(slime-repl-inputed-output-face ((t (:foreground ,iop-fg3))))
   `(slime-repl-output-face ((t (:foreground ,iop-commentfg))))

   ;; tooltip
   `(pos-tip-background-color "#36473A")
   `(pos-tip-foreground-color "#FFFFC8")

   ;; markdown
   `(markdown-blockquote-face ((t (:inherit normal))))
   
   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom iop-light-theme-kit nil
  "Non-nil means load iop-light-theme-kit UI component"
  :type 'boolean
  :group 'iop-light-theme)

(defcustom iop-light-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "iop-light-theme-kit.el")
  "iop-light-theme-kit-file"
  :type 'string
  :group 'iop-light-theme)

(if (and iop-light-theme-kit
         (file-exists-p iop-light-theme-kit-file))
    (load-file iop-light-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'iop-light)

;;; iop-light-theme.el ends here
