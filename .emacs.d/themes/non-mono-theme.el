(deftheme non-mono "My non-mono theme")

(let ((class '((class color) (min-colors 89)))
      ;; non-mono palette
      (non-mono-fg             "#000000")
      (non-mono-cursor-fg      "#000000")
      (non-mono-error          "#302000")
      (non-mono-linebg         "#f0f0f0")
      (non-mono-bg             "#ffffff")
      (non-mono-regionbg       "#c8c8c8")
      (non-mono-bg3            "#909090")
      (non-mono-paren          "#909090")
      (non-mono-grey           "#a0a0a0")
      (non-mono-commentfg      "#383838")

      (non-mono-ui1         "#b0a090")
      (non-mono-ui2         "#656364")
      (non-mono-ui3         "#7fa07f")
      (non-mono-ui4         "#a08888"))
  (custom-theme-set-faces
   'non-mono

   ;; base
   `(default ((t (:background ,non-mono-bg :foreground ,non-mono-fg))))
   `(cursor ((t (:background ,non-mono-cursor-fg :foreground ,non-mono-bg))))
   `(fringe ((t (:foreground ,non-mono-ui2 :background ,non-mono-bg))))
   `(highlight ((t (:background ,non-mono-grey))))
   `(region ((t (:background  ,non-mono-regionbg :foreground ,non-mono-fg))))
   `(warning ((t (:foreground ,non-mono-error))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,non-mono-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-comment-face ((t (:foreground ,non-mono-commentfg))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,non-mono-commentfg))))
   `(font-lock-constant-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-doc-string-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-function-name-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-keyword-face ((t (:foreground ,non-mono-fg ))))
   `(font-lock-negation-char-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-type-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-warning-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-module-def-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-interface-def-face ((t (:foreground ,non-mono-fg))))
   `(font-lock-type-def-face ((t (:foreground ,non-mono-fg))))

   ;; mode line
   `(mode-line-buffer-id ((t (:inherit normal))))
   `(mode-line ((t (:foreground ,non-mono-fg
                                :background ,non-mono-ui3
                                :box nil))))
   `(mode-line-inactive ((t (:foreground ,non-mono-fg
                                         :background ,non-mono-ui2
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,non-mono-fg :background ,non-mono-ui4 :weight normal))))
   `(isearch-fail ((t (:foreground ,non-mono-error :background ,non-mono-bg3))))

   ;; linum-mode
   `(linum ((t (:foreground ,non-mono-commentfg :background ,non-mono-bg))))

   ;; show-paren
   `(show-paren-match ((t (:forground ,non-mono-fg :background ,non-mono-paren))))
   `(show-paren-mismatch ((t (:forground ,non-mono-bg :background ,non-mono-fg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,non-mono-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,non-mono-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,non-mono-linebg)) (t : weight normal)))

   ;; company-mode
   `(company-preview-common ((,class (:foreground ,non-mono-fg :background ,non-mono-bg))))
   `(company-scrollbar-bg ((,class (:background ,non-mono-ui1))))
   `(company-scrollbar-fg ((,class (:background ,non-mono-ui3))))
   `(company-tooltip ((,class (:foreground ,non-mono-fg :background ,non-mono-ui1))))
   `(company-tooltip-common ((,class (:foreground ,non-mono-fg :background ,non-mono-ui4))))
   `(company-tooltip-common-selection ((,class (:foreground ,non-mono-bg :background ,non-mono-fg))))
   `(company-tooltip-selection ((,class (:foreground ,non-mono-bg :background ,non-mono-fg))))
   `(company-tooltip-annotation ((,class (:foreground ,non-mono-fg :background ,non-mono-ui1))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,non-mono-bg :background ,non-mono-fg))))

   ;; ido
   `(ido-first-match ((,class (:foreground ,non-mono-fg :weight bold))))
   `(ido-only-match ((,class (:foreground ,non-mono-fg :weight bold ))))
   `(ido-subdir ((,class (:foreground ,non-mono-fg))))
   `(ido-indicator ((,class (:weight bold :foreground ,non-mono-fg))))
   `(ido-virtual ((,class (:weight bold :foreground ,non-mono-fg))))
   `(ido-incomplete-regexp ((,class (:weight bold :foreground ,non-mono-fg))))

   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,non-mono-fg :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,non-mono-fg))))
   `(font-latex-italic-face ((t (:foreground ,non-mono-fg :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,non-mono-fg))))

   ;; coq
   `(proof-tacticals-name-face ((t (:foreground ,non-mono-fg))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,non-mono-fg))))

   ;; slime
   `(slime-repl-inputed-output-face ((t (:foreground ,non-mono-fg))))
   `(slime-repl-output-face ((t (:foreground ,non-mono-commentfg))))

   ;; tooltip
   `(pos-tip-background-color "#36473A")
   `(pos-tip-foreground-color "#FFFFC8")

   ;; markdown
   `(markdown-blockquote-face ((t (:inherit normal))))

   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom non-mono-theme-kit nil
  "Non-Mono-nil means load non-mono-theme-kit UI component"
  :type 'boolean
  :group 'non-mono-theme)

(defcustom non-mono-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "non-mono-theme-kit.el")
  "non-mono-theme-kit-file"
  :type 'string
  :group 'non-mono-theme)

(if (and non-mono-theme-kit
         (file-exists-p non-mono-theme-kit-file))
    (load-file non-mono-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'non-mono)

;;; non-mono-theme.el ends here
