(deftheme non-blue "My non-blue theme")

(let ((class '((class color) (min-colors 89)))
      ;; non-blue palette
      (non-blue-fg             "#ffffff")
      (non-blue-error          "#302000")
      (non-blue-linebg         "#202030")
      (non-blue-bg             "#000080")
      (non-blue-regionbg       "#006060")
      (non-blue-bg3            "#909090")
      (non-blue-paren          "#60b060")
      (non-blue-grey           "#a0a0a0")
      (non-blue-commentfg      "#00ffff")

      (non-blue-ui1         "#b0a090")
      (non-blue-ui2         "#000000")
      (non-blue-ui3         "#008080")
      (non-blue-ui4         "#a08888"))
  (custom-theme-set-faces
   'non-blue

   ;; base
   `(default ((t (:background ,non-blue-bg :foreground ,non-blue-fg))))
   `(cursor ((t (:background ,non-blue-fg :foreground ,non-blue-bg))))
   `(fringe ((t (:foreground ,non-blue-ui2 :background ,non-blue-bg))))
   `(highlight ((t (:background ,non-blue-grey))))
   `(region ((t (:background  ,non-blue-regionbg :foreground ,non-blue-fg))))
   `(warning ((t (:foreground ,non-blue-error))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,non-blue-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-comment-face ((t (:foreground ,non-blue-commentfg))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,non-blue-commentfg))))
   `(font-lock-constant-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-doc-string-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-function-name-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-keyword-face ((t (:foreground ,non-blue-fg ))))
   `(font-lock-negation-char-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-type-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-warning-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-module-def-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-interface-def-face ((t (:foreground ,non-blue-fg))))
   `(font-lock-type-def-face ((t (:foreground ,non-blue-fg))))

   ;; mode line
   `(mode-line ((t (:foreground ,non-blue-fg
                                :background ,non-blue-ui3
                                :box nil))))
   `(mode-line-inactive ((t (:foreground ,non-blue-fg
                                         :background ,non-blue-ui2
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,non-blue-fg :background ,non-blue-ui4 :weight normal))))
   `(isearch-fail ((t (:foreground ,non-blue-error :background ,non-blue-bg3))))

   ;; linum-mode
   `(linum ((t (:foreground ,non-blue-commentfg :background ,non-blue-bg))))

   ;; show-paren
   `(show-paren-match ((t (:forground ,non-blue-fg :background ,non-blue-paren))))
   `(show-paren-mismatch ((t (:forground ,non-blue-bg :background ,non-blue-fg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,non-blue-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,non-blue-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,non-blue-linebg)) (t : weight normal)))

   ;; company-mode
   `(company-preview-common ((,class (:foreground ,non-blue-fg :background ,non-blue-bg))))
   `(company-scrollbar-bg ((,class (:background ,non-blue-ui1))))
   `(company-scrollbar-fg ((,class (:background ,non-blue-ui3))))
   `(company-tooltip ((,class (:foreground ,non-blue-fg :background ,non-blue-ui1))))
   `(company-tooltip-common ((,class (:foreground ,non-blue-fg :background ,non-blue-ui4))))
   `(company-tooltip-common-selection ((,class (:foreground ,non-blue-bg :background ,non-blue-fg))))
   `(company-tooltip-selection ((,class (:foreground ,non-blue-bg :background ,non-blue-fg))))
   `(company-tooltip-annotation ((,class (:foreground ,non-blue-fg :background ,non-blue-ui1))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,non-blue-bg :background ,non-blue-fg))))

   ;; ido
   `(ido-first-match ((,class (:foreground ,non-blue-fg :weight bold))))
   `(ido-only-match ((,class (:foreground ,non-blue-fg :weight bold ))))
   `(ido-subdir ((,class (:foreground ,non-blue-fg))))
   `(ido-indicator ((,class (:weight bold :foreground ,non-blue-fg))))
   `(ido-virtual ((,class (:weight bold :foreground ,non-blue-fg))))
   `(ido-incomplete-regexp ((,class (:weight bold :foreground ,non-blue-fg))))

   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,non-blue-fg :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,non-blue-fg))))
   `(font-latex-italic-face ((t (:foreground ,non-blue-fg :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,non-blue-fg))))

   ;; coq
   `(proof-tacticals-name-face ((t (:foreground ,non-blue-fg))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,non-blue-fg))))

   ;; slime
   `(slime-repl-inputed-output-face ((t (:foreground ,non-blue-fg))))
   `(slime-repl-output-face ((t (:foreground ,non-blue-commentfg))))

   ;; tooltip
   `(pos-tip-background-color "#36473A")
   `(pos-tip-foreground-color "#FFFFC8")

   ;; markdown
   `(markdown-blockquote-face ((t (:inherit normal))))

   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom non-blue-theme-kit nil
  "Non-Blue-nil means load non-blue-theme-kit UI component"
  :type 'boolean
  :group 'non-blue-theme)

(defcustom non-blue-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "non-blue-theme-kit.el")
  "non-blue-theme-kit-file"
  :type 'string
  :group 'non-blue-theme)

(if (and non-blue-theme-kit
         (file-exists-p non-blue-theme-kit-file))
    (load-file non-blue-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'non-blue)

;;; non-blue-theme.el ends here
