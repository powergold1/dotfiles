(deftheme poi-light "My poi-light theme")

(let ((class '((class color) (min-colors 89)))
      ;; poi palette
      (poi-fg             "#000000")
      (poi-pink           "#fc6786")
      (poi-orange         "#904030")
      (poi-wheat          "#d4ce99")
      (poi-blue           "#000057")
      (poi-darkblue       "#161660")
      (poi-lightblue      "#162580")
      (poi-green          "#104000")
      (poi-darkwine       "#1e0010")
      (poi-wine           "#960050")
      (poi-purple         "#9e71df")
      (poi-palevioletred  "#d33682")
      (poi-darkred        "#67003f")
      (poi-grey-2         "#808080")
      (poi-grey           "#505050")
      (poi-grey+2         "#d0d0d0")
      (poi-grey+5         "#c0c0c0")
      ;; (poi-bg             "#f6f4ca")
      ;; (poi-bg             "#eef2ff")
      ;; (poi-bg             "#d6daf0")
      ;; (poi-bgalt          "#fcf4c1")
      ;; (poi-bgalt          "#c1f4fd")
      
      ;; (poi-bg             "#fdf4c1")
      (poi-bg             "#e8e8f8")
      ;;(poi-bgalt          "#d6daf0")
      (poi-linebg         "#d6f0d6")
      (poi-bgalt          "#f0faca")
      (poi-dark           "#000000")
      (poi-base01         "#465457")
      (poi-base02         "#989883")
      (poi-base03         "#d8d8c3"))
  (custom-theme-set-faces
   'poi-light
   

   ;; base
   `(default ((t (:background ,poi-bg :foreground ,poi-fg))))
   `(cursor ((t (:background ,poi-fg :foreground ,poi-bg))))
   `(fringe ((t (:foreground ,poi-base02 :background ,poi-bg))))
   `(highlight ((t (:background ,poi-grey))))
   `(region ((t (:background  ,poi-bgalt))
             (t :inverse-video t)))
   `(warning ((t (:foreground ,poi-palevioletred))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,poi-darkred))))
   `(font-lock-comment-face ((t (:foreground ,poi-grey))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,poi-grey))))
   `(font-lock-constant-face ((t (:foreground ,poi-blue))))
   `(font-lock-doc-string-face ((t (:foreground ,poi-green))))
   `(font-lock-function-name-face ((t (:foreground ,poi-darkred))))
   `(font-lock-keyword-face ((t (:foreground ,poi-darkred ))))
   `(font-lock-negation-char-face ((t (:foreground ,poi-wine))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,poi-green))))
   `(font-lock-type-face ((t (:foreground ,poi-darkred))))
   `(font-lock-variable-name-face ((t (:foreground ,poi-blue))))
   `(font-lock-warning-face ((t (:foreground ,poi-palevioletred))))

   ;; mode line
   `(mode-line ((t (:foreground ,poi-fg
                                :background ,poi-base03
                                :box nil))))
   `(mode-line-buffer-id ((t (:weight normal))))
   `(mode-line-inactive ((t (:foreground ,poi-fg
                                         :background ,poi-base02
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,poi-dark :background ,poi-wheat :weight normal))))
   `(isearch-fail ((t (:foreground ,poi-wine :background ,poi-darkwine))))

   ;; linum-mode
   `(linum ((t (:foreground ,poi-grey-2 :background ,poi-bg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,poi-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,poi-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,poi-linebg)) (t : weight normal)))
   
   ;; company-mode
   `(company-preview-common ((,class (:foreground nil :background ,poi-purple))))
   `(company-scrollbar-bg ((,class (:background ,poi-base03))))
   `(company-scrollbar-fg ((,class (:background ,poi-grey+5))))
   `(company-tooltip ((,class (:foreground ,poi-fg :background ,poi-base03))))
   `(company-tooltip-common ((,class (:foreground ,poi-green :background ,poi-base03))))
   `(company-tooltip-common-selection ((,class (:foreground ,poi-green :background ,poi-wheat))))
   `(company-tooltip-selection ((,class (:background ,poi-wheat))))

   ;;
   `(proof-tacticals-name-face ((t (:foreground ,poi-darkblue))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,poi-lightblue))))

   
   ;; TODO
   ;; ido-mode
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom poi-light-theme-kit nil
  "Non-nil means load poi-light-theme-kit UI component"
  :type 'boolean
  :group 'poi-light-theme)

(defcustom poi-light-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "poi-light-theme-kit.el")
  "poi-light-theme-kit-file"
  :type 'string
  :group 'poi-light-theme)

(if (and poi-light-theme-kit
         (file-exists-p poi-light-theme-kit-file))
    (load-file poi-light-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'poi-light)

;;; poi-light-theme.el ends here
