(deftheme non-pray "My non-pray theme")

(let ((class '((class color) (min-colors 89)))
      ;; non palette
      (non-fg             "#191970")
      (non-error          "#302000")
      (non-linebg         "#202030")
      (non-bg             "#fafae0")
      (non-regionbg       "#eee8aa")
      (non-bg3            "#909090")
      (non-paren          "#70c070")
      (non-grey           "#a0a0a0")
      (non-commentfg      "#383838")

      (non-ui1         "#b0a090")
      (non-ui2         "#a0a0a0")
      (non-ui3         "#f0e68c")
      (non-ui4         "#c8d898"))
  (custom-theme-set-faces
   'non-pray

   ;; base
   `(default ((t (:background ,non-bg :foreground ,non-fg))))
   `(cursor ((t (:background ,non-fg :foreground ,non-bg))))
   `(fringe ((t (:foreground ,non-ui2 :background ,non-bg))))
   `(highlight ((t (:background ,non-grey))))
   `(region ((t (:background  ,non-regionbg :foreground ,non-fg))))
   `(warning ((t (:foreground ,non-error))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,non-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,non-fg))))
   `(font-lock-comment-face ((t (:foreground ,non-commentfg))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,non-commentfg))))
   `(font-lock-constant-face ((t (:foreground ,non-fg))))
   `(font-lock-doc-string-face ((t (:foreground ,non-fg))))
   `(font-lock-function-name-face ((t (:foreground ,non-fg))))
   `(font-lock-keyword-face ((t (:foreground ,non-fg ))))
   `(font-lock-negation-char-face ((t (:foreground ,non-fg))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,non-fg))))
   `(font-lock-type-face ((t (:foreground ,non-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,non-fg))))
   `(font-lock-warning-face ((t (:foreground ,non-fg))))
   `(font-lock-module-def-face ((t (:foreground ,non-fg))))
   `(font-lock-interface-def-face ((t (:foreground ,non-fg))))
   `(font-lock-type-def-face ((t (:foreground ,non-fg))))

   ;; mode line
   `(mode-line-buffer-id ((t (:inherit normal))))
   `(mode-line ((t (:foreground ,non-fg
                                :background ,non-ui3
                                :box nil))))
   `(mode-line-inactive ((t (:foreground ,non-fg
                                         :background ,non-ui2
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,non-fg :background ,non-ui4 :weight normal))))
   `(isearch-fail ((t (:foreground ,non-error :background ,non-bg3))))

   ;; show-paren
   `(show-paren-match ((t (:forground ,non-fg :background ,non-paren))))
   `(show-paren-mismatch ((t (:forground ,non-bg :background ,non-fg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,non-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,non-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,non-linebg)) (t : weight normal)))

   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,non-fg :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,non-fg))))
   `(font-latex-italic-face ((t (:foreground ,non-fg :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,non-fg))))

   ;; coq
   `(proof-tacticals-name-face ((t (:foreground ,non-fg))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,non-fg))))

   ;; slime
   `(slime-repl-inputed-output-face ((t (:foreground ,non-fg))))
   `(slime-repl-output-face ((t (:foreground ,non-commentfg))))

   ;; tooltip
   `(pos-tip-background-color "#36473A")
   `(pos-tip-foreground-color "#FFFFC8")

   ;; markdown
   `(markdown-blockquote-face ((t (:inherit normal))))

   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom non-theme-kit nil
  "Non-nil means load non-theme-kit UI component"
  :type 'boolean
  :group 'non-theme)

(defcustom non-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "non-theme-kit.el")
  "non-pray-theme-kit-file"
  :type 'string
  :group 'non-theme)

(if (and non-theme-kit
         (file-exists-p non-theme-kit-file))
    (load-file non-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'non-pray)

;;; non-pray-theme.el ends here
