(deftheme non-ugly "My non-ugly theme")

(let ((class '((class color) (min-colors 89)))
      ;; non-ugly palette
      (non-ugly-fg             "#006400")
      (non-ugly-error          "#302000")
      (non-ugly-linebg         "#202030")
      (non-ugly-bg             "#ffd700")
      (non-ugly-regionbg       "#d4ae29")
      (non-ugly-bg3            "#909090")
      (non-ugly-paren          "#b06060")
      (non-ugly-grey           "#a0a0a0")
      (non-ugly-commentfg      "#205020")

      (non-ugly-ui1         "#b0a090")
      (non-ugly-ui2         "#a07f10")
      (non-ugly-ui3         "#f0af10")
      (non-ugly-ui4         "#a08888"))
  (custom-theme-set-faces
   'non-ugly

   ;; base
   `(default ((t (:background ,non-ugly-bg :foreground ,non-ugly-fg))))
   `(cursor ((t (:background ,non-ugly-fg :foreground ,non-ugly-bg))))
   `(fringe ((t (:foreground ,non-ugly-ui2 :background ,non-ugly-bg))))
   `(highlight ((t (:background ,non-ugly-grey))))
   `(region ((t (:background  ,non-ugly-regionbg :foreground ,non-ugly-fg))))
   `(warning ((t (:foreground ,non-ugly-error))))
   `(minibuffer-prompt ((,class (:weight bold :foreground ,non-ugly-fg))))

   ;; font lock
   `(font-lock-builtin-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-comment-face ((t (:foreground ,non-ugly-commentfg))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,non-ugly-commentfg))))
   `(font-lock-constant-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-doc-string-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-function-name-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-keyword-face ((t (:foreground ,non-ugly-fg ))))
   `(font-lock-negation-char-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-preprocessor-face ((t (:inherit (font-lock-builtin-face)))))
   `(font-lock-regexp-grouping-backslash ((t (:inherit (normal)))))
   `(font-lock-regexp-grouping-construct ((t (:inherit (normal)))))
   `(font-lock-string-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-type-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-variable-name-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-warning-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-module-def-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-interface-def-face ((t (:foreground ,non-ugly-fg))))
   `(font-lock-type-def-face ((t (:foreground ,non-ugly-fg))))

   ;; mode line
   `(mode-line ((t (:foreground ,non-ugly-fg
                                :background ,non-ugly-ui3
                                :box nil))))
   `(mode-line-inactive ((t (:foreground ,non-ugly-fg
                                         :background ,non-ugly-ui2
                                         :box nil))))

   ;; search
   `(isearch ((t (:foreground ,non-ugly-fg :background ,non-ugly-ui4 :weight normal))))
   `(isearch-fail ((t (:foreground ,non-ugly-error :background ,non-ugly-bg3))))

   ;; linum-mode
   `(linum ((t (:foreground ,non-ugly-commentfg :background ,non-ugly-bg))))

   ;; show-paren
   `(show-paren-match ((t (:forground ,non-ugly-fg :background ,non-ugly-paren))))
   `(show-paren-mismatch ((t (:forground ,non-ugly-bg :background ,non-ugly-fg))))

   ;; hl-line-mode
   `(hl-line-face ((,class (:background ,non-ugly-linebg)) (t :weight normal)))
   `(hl-line ((,class (:background ,non-ugly-linebg)) (t :weight normal)))
   `(highlight-current-line-face ((,class (:background ,non-ugly-linebg)) (t : weight normal)))

   ;; company-mode
   `(company-preview-common ((,class (:foreground ,non-ugly-fg :background ,non-ugly-bg))))
   `(company-scrollbar-bg ((,class (:background ,non-ugly-ui1))))
   `(company-scrollbar-fg ((,class (:background ,non-ugly-ui3))))
   `(company-tooltip ((,class (:foreground ,non-ugly-fg :background ,non-ugly-ui1))))
   `(company-tooltip-common ((,class (:foreground ,non-ugly-fg :background ,non-ugly-ui4))))
   `(company-tooltip-common-selection ((,class (:foreground ,non-ugly-bg :background ,non-ugly-fg))))
   `(company-tooltip-selection ((,class (:foreground ,non-ugly-bg :background ,non-ugly-fg))))
   `(company-tooltip-annotation ((,class (:foreground ,non-ugly-fg :background ,non-ugly-ui1))))
   `(company-tooltip-annotation-selection ((,class (:foreground ,non-ugly-bg :background ,non-ugly-fg))))

   ;; ido
   `(ido-first-match ((,class (:foreground ,non-ugly-fg :weight bold))))
   `(ido-only-match ((,class (:foreground ,non-ugly-fg :weight bold ))))
   `(ido-subdir ((,class (:foreground ,non-ugly-fg))))
   `(ido-indicator ((,class (:weight bold :foreground ,non-ugly-fg))))
   `(ido-virtual ((,class (:weight bold :foreground ,non-ugly-fg))))
   `(ido-incomplete-regexp ((,class (:weight bold :foreground ,non-ugly-fg))))

   ;; auctex
   `(font-latex-normal-face ((t (:inherit normal))))
   `(font-latex-warning-face ((t (:inherit font-lock-warning-face :slant normal))))
   `(font-latex-sectioning-1-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-sectioning-2-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-sectioning-3-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-sectioning-4-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-sectioning-5-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-slide-title-face ((t (:foreground ,non-ugly-fg :weight normal ))))
   `(font-latex-sedate-face ((t (:foreground ,non-ugly-fg))))
   `(font-latex-italic-face ((t (:foreground ,non-ugly-fg :slant italic))))
   `(font-latex-string-face ((t (:inherit ,font-lock-string-face))))
   `(font-latex-math-face ((t (:foreground ,non-ugly-fg))))

   ;; coq
   `(proof-tacticals-name-face ((t (:foreground ,non-ugly-fg))))
   `(coq-solve-tactics-face ((t (:inherit proof-tacticals-name-face))))
   `(proof-tactics-name-face ((t (:foreground ,non-ugly-fg))))

   ;; slime
   `(slime-repl-inputed-output-face ((t (:foreground ,non-ugly-fg))))
   `(slime-repl-output-face ((t (:foreground ,non-ugly-commentfg))))

   ;; tooltip
   `(pos-tip-background-color "#36473A")
   `(pos-tip-foreground-color "#FFFFC8")

   ;; markdown
   `(markdown-blockquote-face ((t (:inherit normal))))

   ;; TODO
   ;; flycheck
   ;; show-paren
   ;; rainbow-delimiters
   ;; highlight-symbols
   ))

(defcustom non-ugly-theme-kit nil
  "Non-Ugly-nil means load non-ugly-theme-kit UI component"
  :type 'boolean
  :group 'non-ugly-theme)

(defcustom non-ugly-theme-kit-file
  (concat (file-name-directory
           (or (buffer-file-name) load-file-name))
          "non-ugly-theme-kit.el")
  "non-ugly-theme-kit-file"
  :type 'string
  :group 'non-ugly-theme)

(if (and non-ugly-theme-kit
         (file-exists-p non-ugly-theme-kit-file))
    (load-file non-ugly-theme-kit-file))

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'non-ugly)

;;; non-ugly-theme.el ends here
